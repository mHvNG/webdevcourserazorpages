using System;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Carter;
using Microsoft.AspNetCore.Http;

namespace Exercises.Pages.Lesson0 {
    public class Exercises : CarterModule {
        public Exercises() {
            Get("/Lesson0/assignment0", Assignment0);

            Get("/Lesson0/assignment1", Assignment1);

            Get("/Lesson0/assignment2", Assignment2);

            Get("/Lesson0/assignment3", Assignment3);

            Get("/Lesson0/assignment4", Assignment4);

            Get("/Lesson0/assignment5", Assignment5Get);

            Post("/Lesson0/assignment5", Assignment5Post);
        }

        private Task Assignment0(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/plain";
            response.WriteAsync("Hello World");
            return Task.CompletedTask;
        }

        private Task Assignment1(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/html";
            response.WriteAsync("<h1>Hello World!</h1>");
            return Task.CompletedTask;
        }

        private Task Assignment2(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/html";
            string query = request.QueryString.ToString();
            switch(request.QueryString.HasValue) {
                case true:
                    // Ik gebruik Length voor een reden, aangezien de vergelijking `query.Split("=")[1] == ""` een geheugen slot opent.
                    // En deze oplossing is een sneller process.
                    if (query.Length <= 1 || query.Split("=")[1].Length == 0) {
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return Task.CompletedTask;
                    }
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response.WriteAsync($"<h1>Hello {query.Split('=')[1]}</h1>");
                case false:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
            }
            return Task.CompletedTask;
        }

        private Task Assignment3(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/html";
            if (request.QueryString.ToString().Length <= 1) { response.StatusCode = (int)HttpStatusCode.BadRequest; }
            string[] keys = { "name", "age" };
            List<string> values = new List<string>();
            restart:
            foreach (var key in keys) {
                if (values.Count > 0) {
                    int indx = 0;
                    foreach (var name in request.Query[key]) {
                        values.Add(request.Query[key][indx]);
                        indx++;
                    }
                    break;
                }
                if (key.Equals("age") && values.Count < 1) {
                    values.Add(request.Query[key]);
                    goto restart;
                }
            }
            response.ContentType = "text/html";
            return response.WriteAsync($"<ul>Leeftijd: {values[0]} van <li>{values[1]}</li><li>{values[2]}</li></ul>");
        }

        private Task Assignment4(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/html";
            string query = WebUtility.UrlDecode(request.QueryString.ToString());
            if (query.Length <= 1) { response.StatusCode = (int)HttpStatusCode.BadRequest; }
            if (query.Contains("=") && query.Split("=")[1].Length == 0)
                response.StatusCode = (int)HttpStatusCode.BadRequest;
            else if (!query.Contains("="))
                response.StatusCode = (int)HttpStatusCode.BadRequest;
            string value = query.Split("=")[1];
            string[] keywords = { "<script>", "</script>" };
            foreach (var keyword in keywords) {
                if (!value.Contains(keyword)) {
                    return response.WriteAsync("<h1>No JavaScript query given...</h1><br><h4>Nothing to decode...</h4>");
                }
            }
            /* Decode the javascript code. */
            string newUrl = WebUtility.UrlEncode(query);
            return response.WriteAsync($"<h1>JavaScript query given...</h1><br><h4>Javascript detected! Url query has been decoded... Decoded query - {newUrl}</h4>");
        }

        private Task Assignment5Get(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/html";
            return response.WriteAsync(this.FormAssignment5());
        }

        private Task Assignment5Post(HttpRequest request, HttpResponse response) {
            response.StatusCode = (int)HttpStatusCode.OK;
            response.ContentType = "text/html";
            List<string> errorMessages = new List<string> { };
            List<string> values = new List<string> { };
            string[] keys = { "fname", "lname", "age" };
            foreach (string key in keys) {
                if (request.Form.ContainsKey(key)) {
                    int tempInt = 0;
                    float tempFloat = 0.0f;
                    if (int.TryParse(request.Form[key].ToString(), out tempInt)) {
                        values.Add(tempInt.ToString());
                        errorMessages.Add("");
                        continue;
                    } else if (float.TryParse(request.Form[key].ToString(), out tempFloat)) {
                        values.Add(tempFloat.ToString());
                        errorMessages.Add("Given age not a whole number...");
                        continue;
                    }
                    if (request.Form[key].ToString().Length > 1) {
                        values.Add(request.Form[key].ToString());
                        errorMessages.Add("");
                    } else {
                        values.Add(request.Form[key].ToString());
                        errorMessages.Add("Names requires two or more characters...");
                    }
                } else { return response.WriteAsync(""); }
            }
            return response.WriteAsync(this.FormAssignment5(firstName: values[0], lastName: values[1], age: values[2], 
                firstNameError: errorMessages[0], lastNameError: errorMessages[1], ageError: errorMessages[2]));
        }

        private string FormAssignment5(string firstName = "", string lastName = "", string age = "", string firstNameError = "", string lastNameError = "", string ageError = "") {
            var form = $@"
                <form action='assignment5' method='POST'>
                    <label for='fname'>First name:</label><br>
                    <input type='text' name='fname' value={firstName}>
                    <label for='fname'>{firstNameError}</label><br>
                    <label for='lname'>Last name:</label><br>
                    <input type='text' name='lname' value={lastName}>
                    <label for='lname'>{lastNameError}</label><br>
                    <label for='age'>Age:</label><br>
                    <input type='text' name='age' value={age}>
                    <label for='age'>{ageError}</label><br>
                    <input type='submit' value='Submit'>
                </form>";
            return form;
        }
    }
}
